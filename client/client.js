'use strict';

angular.module('clientApp', [
        'ngAnimate',
        'ngResource',
        'ui.router',
        'ngMaterial',
        'LocalStorageModule',
        'angular-loading-bar',
        'ngMessages',
        'underscore',
        

        'app.config',
        'mod.nbos',
        'mod.idn',
        'mod.app',
        {{#if module_includes}}
          {{{module_includes}}}
        {{/if}}
])

angular.module('clientApp')
.constant('CLIENT_CONFIG',{
        CLIENT_ID: '',
        CLIENT_SECRET: '',
        CLIENT_DOMAIN : 'http://localhost:9001'
})
